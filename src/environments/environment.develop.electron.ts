export const environment = {
	production: true,
	vaultBaseUrl: 'vault-test.kominal.com',
	ecosystemBaseUrl: 'ecosystem-test.kominal.com',
	isElectron: true,
	commit: 'COMMIT',
};
