export const environment = {
	production: true,
	vaultBaseUrl: 'vault.kominal.com',
	ecosystemBaseUrl: 'ecosystem.kominal.com',
	isElectron: true,
	commit: 'COMMIT',
};
