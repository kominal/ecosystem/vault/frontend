export const environment = {
	production: true,
	vaultBaseUrl: 'vault.kominal.com',
	ecosystemBaseUrl: 'ecosystem.kominal.com',
	isElectron: false,
	commit: 'COMMIT',
};
