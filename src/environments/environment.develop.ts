export const environment = {
	production: true,
	vaultBaseUrl: 'vault-test.kominal.com',
	ecosystemBaseUrl: 'ecosystem-test.kominal.com',
	isElectron: false,
	commit: 'COMMIT',
};
